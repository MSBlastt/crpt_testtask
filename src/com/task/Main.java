package com.task;

import java.math.BigInteger;

/**
 * Test Task for CRPT.
 * Calculates fibonacci Nth number value and its number of trailing zeroes
 *
 * Created by Mikhail Kholodkov
 *         on March 29, 2018
 */
public class Main {

    public static void main(String[] args) {
        ConsoleUtils.printToConsole("*** Program determines number of 0 (zeroes) in the end of Nth Fibonacci's sequence number ***");

        ConsoleUtils.printEmptyLine();

        String input;

        // Getting Nth sequence number input
        do {
            input = requestNumberInput();
        } while (!isValidInput(input));

        ConsoleUtils.printEmptyLine();

        // Input is valid. Calculating Fibonacci's sequence value
        BigInteger fibonacciValue = getFibonacci(input);
        ConsoleUtils.printToConsole("Fibonacci value is: " + fibonacciValue.toString());

        // Calculating number of trailing zeroes
        int count = getNumberOfTrailingZeroes(fibonacciValue);

        ConsoleUtils.printEmptyLine();

        // Printing final answer
        ConsoleUtils.printToConsole("Calculated value has " + count + " trailing zeroes.");
    }

    private static String requestNumberInput() {
        ConsoleUtils.printToConsole("Please enter Fibonacci's sequence number: ");
        
        return ConsoleUtils.readFromConsole();
    }

    private static boolean isValidInput(String input) {
        boolean validInput = input != null && input.trim().matches("^[1-9]\\d{0,8}$");

        if (!validInput) {
            ConsoleUtils.printToConsole("Invalid value. Please provide number within [1, 999999999] range. Thank you.");
            ConsoleUtils.printEmptyLine();
        }

        return validInput;
    }

    private static BigInteger getFibonacci(String input) {
        return FibonacciCalc.fastFibonacciDoubling(Integer.valueOf(input));
    }

    private static int getNumberOfTrailingZeroes(BigInteger value) {
        int count = 0;

        while (value.divideAndRemainder(BigInteger.TEN)[1].equals(BigInteger.ZERO)) {
            value = value.divide(BigInteger.TEN);
            count++;
        }

        return count;
    }
}
