package com.task;

import java.util.Scanner;

/**
 * Console Util class.
 * Can be used for different console manipulations.
 *
 * Created by Mikhail Kholodkov
 *         on March 29, 2018
 */
public class ConsoleUtils {

    private static final Scanner CONSOLE_READER = new Scanner(System.in);

    public static void printToConsole(String text) {
        System.out.print(text);
    }

    public static String readFromConsole() {
        return CONSOLE_READER.next();
    }

    public static void printEmptyLine() {
        System.out.println(System.getProperty("line.separator"));
    }
}
